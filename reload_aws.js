function reloadAmazonTabs() {
  chrome.tabs.query({}, function(tabs) {
    tabs.map((tab)=>{
      if(tab.url.indexOf('console.aws.amazon.com') !== -1) {
        chrome.tabs.reload(tab.id, {}, null);
      }
    })
  });
}

chrome.browserAction.onClicked.addListener(function() {
  reloadAmazonTabs();
});
