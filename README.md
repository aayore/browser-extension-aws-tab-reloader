# Browser Extension: AWS Tab Reloader

The 12-hour console session timeout.  Spending three - maybe even as many as seven - whole minutes every morning getting back to where you were.  Discovering a leftover tab at 3:00pm that you forgot to reload in the morning.  Oh, the humanity!

Update: This problem is significantly worse if you're using multiple accounts and frequently switching roles.  I really wish Amazon would provide an auto-reload feature for the console.  But this plugin/extension helps.

This extension provides a button that reloads all your AWS tabs.  One click, all reloaded.  Done and done.  After you've logged into one tab.  But then done.


## Installation

Until this is packaged and released more professionally...

### Chrome:

[Installation instructions](https://developer.chrome.com/extensions/getstarted)

### Firefox:

[Installation instructions](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Your_first_WebExtension)
